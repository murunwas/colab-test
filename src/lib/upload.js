const uploadMiddleware = async (req, res, next) =>{
    console.log(req.method);
    
    if (req.url.startsWith('/upload')) {
        /* Uploads the files */
        try {
            return res.end('ok');
        } catch (err) {
            res.writeHead(500, {
                'Content-Type': 'text/plain',
            });
            return res.end('upload plugin error: ' + err);
        }
    } else {
        /* Other URL: continue... */
        next();
    }
}


const uploadPlugin = {
    name: 'upload-middleware',
    configureServer (server) {
        server.middlewares.use(uploadMiddleware);
    },
};

export {uploadMiddleware, uploadPlugin};