import busboy from 'busboy'
import fs  from "fs"
import { randomFillSync  } from "crypto";
import path from "path";
import os from "os";

const base = 'https://api.svelte.dev';

const random = (() => {
	const buf = Buffer.alloc(16);
	return () => randomFillSync(buf).toString('hex');
  })();

export async function api(request: Request, resource: string, data?: Record<string, unknown>) {
	return fetch(`${base}/${resource}`, {
		method: request.method,
		headers: {
			'content-type': 'application/json'
		},
		body: data && JSON.stringify(data)
	});
}


export async function apiUpload(request: Request):Promise<boolean> {
	return new Promise((resolve,reject)=>{
		const bb = busboy({ headers: request.headers });
		bb.on('file', (name, file, info) => {
		  const saveTo = path.join(os.tmpdir(), `busboy-upload-${random()}`);
		  console.log(saveTo);
		  
		  file.pipe(fs.createWriteStream(saveTo));
		});
		bb.on('close', () => {
			resolve(true);
		});

		bb.on('error', err => reject(false));
		//request.pipe(bb);
	})
}
